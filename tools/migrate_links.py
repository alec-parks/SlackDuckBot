# -*- coding: utf-8 -*-
import db
import psycopg2
import config
import requests
import util
import time

if __name__ == "__main__":
    with db.get_connection() as conn:

                    data = None
                    with conn.cursor() as curs:
                        try:
                            curs.execute("ALTER TABLE links ADD COLUMN title text;")
                            curs.execute("DELETE FROM links WHERE link LIKE '@%';")
                            curs.execute("DELETE FROM links WHERE link LIKE '#%';")

                            curs.execute("SELECT * FROM links;")
                            data = curs.fetchall()

                        except psycopg2.Error as e:
                            print e.pgerror

                    for rowid, username, link, message, channel, title in data:
                        with conn.cursor() as curs:
                            try:
                                title = util.getTitle(link)
                                print link + ": " + unicode(title)
                                message = util.replaceIdsInMessage(message)
                                curs.execute("""UPDATE links 
                                                    SET message = %(message)s,
                                                    title = %(title)s
                                               WHERE id = %(rowid)s;""", {"message": message, "rowid": int(rowid), "title": title})
                            except psycopg2.Error as e:
                                print e.pgerror
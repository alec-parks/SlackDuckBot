from __future__ import absolute_import
import random

class Quack(object):
    """Quacks."""

    def on_message(self, channel, user, message, s):
        if random.random() < 0.01:
            s.send_msg("Quack", channel)
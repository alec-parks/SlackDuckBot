from plugins.help import Help
from plugins.quack import Quack

PLUGINS = [Help, Quack]

# Your slack token
SLACK_TOKEN = ""

# MS Azure Key, needed for ShowImage
BING_API_KEY = ""

# repl.it api key, needs to be a bytestring
# needed for Replit
REPLIT_API_KEY = b""

# Commands need to be prefixed with this character
COMMAND_LEADER = "!"

# The bots own name
BOT_NAME = "linkhoarder"

# Please refer to http://docs.sqlalchemy.org/en/latest/core/engines.html#database-urls
DB_CONNECTION = 'sqlite:///:memory:'

# Where finished webm thumbnails are stored
THUMB_DIR = ""
# The URL under they are served
THUMB_WEB_DIR = ""